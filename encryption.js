'use strict';
const crypto = require('crypto');
const AES_METHOD = 'aes-256-cbc';
const IV_LENGTH = 16; // For AES, this is always 16, checked with php
var plaintext = jQuery.param(userobject);
 plaintext = http_build_query($parametre);
// plain userobject data
console.log(plaintext);

function encrypt(text, password) {
    if (process.versions.openssl <= '1.0.1f') {
        throw new Error('OpenSSL Version too old, vulnerability to Heartbleed')
    }

    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv(AES_METHOD, new Buffer(password), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
    let textParts = text.split(':');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
}


(function(){ 
    const key ='3b541db1-8742-4110-abc8-027c0ac71bb7';
  var userobject = {
    'apiKey': '00d82dc1-c056-4889-a4f1-5af8d9e23886',
    'brugerUuid': '80457b8c-c47f-47ca-a0c5-d6965a3cd02a';
    'cpr': '0902991234',
    'fornavn': 'Erik',
    'efternavn': 'Helweg Larsen',
    'email': 'ehl@kiatec.dk',
    'telefon': '61502371',
}
    var encryptdata = encrypt(userobject,key);
    console.log(encryptdata);
})(); 
