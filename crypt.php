<?php
//$key previously generated safely, ie: openssl_random_pseudo_bytes

$key='3b541db1-8742-4110-abc8-027c0ac71bb7';

/*
SYSTEM-ID (Som repræsenterer CareX) – er altid udfyldt, og altid den samme værdi.
CareX UUID som identificerer brugeren – er stabil for en given bruger, men kan variere ift. CPR nummer (samme CPR nummer kan have flere brugere)
CPR nummer – Når det oversendes er det valideret mod NemID
Fornavn – Kan være selv-angivet, og dermed ikke nødvendigvis ”korrekt” i forhold til den fysiske person.
Efternavn – Kan være selv-angivet, og dermed ikke nødvendigvis ”korrekt” i forhold til den fysiske person.
e-mail – kan være tomt. Der vil ikke være angivet en tekst som repræsenterer ”tom”
mobilnummer – kan være tomt. Der vil ikke være angivet en tekst som repræsenterer ”tom”
*/

$parametre['apiKey']='00d82dc1-c056-4889-a4f1-5af8d9e23886';
$parametre['brugerUuid']='80457b8c-c47f-47ca-a0c5-d6965a3cd02a';
$parametre['cpr']='0902991234';
$parametre['fornavn']='Erik';
$parametre['efternavn']='Helweg Larsen';
$parametre['email']='ehl@kiatec.dk';
$parametre['telefon']='61502371';

$plaintext = http_build_query($parametre);

echo "før dekryptering \n\n";
echo $plaintext."\n";



$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
$iv = openssl_random_pseudo_bytes($ivlen);
$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
echo "\n\n denne streng udveksles \n\n";

echo $ciphertext;

//decrypt later....
$c = base64_decode($ciphertext);
$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
$iv = substr($c, 0, $ivlen);
$hmac = substr($c, $ivlen, $sha2len=32);
$ciphertext_raw = substr($c, $ivlen+$sha2len);
$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
{
    echo "\n\n efter dekryptering \n\n";
    echo $original_plaintext."\n";
}
?>
