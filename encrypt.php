<?php

$plaintext = '[{"clientKey":"c32f7123-a9c7-40a5-9096-1f1d9c3e6149"},{"personUuid":"4b371ae5-7af5-4980-b827-2c99fe3fbc77"},{"cpr":"12345678"},{"fornavn":"Test"},{"efternavn":"Testesen"},{"email":"appsupport@kiatec.dk"},{"telefon":"123243434343"},{"orgcvr":"33259247"},{"orgnavn":"GJENSIDIGE FORSIKRING, DANSK FILIAL AF GJENSIDIGE FORSIKRING ASA, NORGE"},[{"serviceaftaleid":"GS-PRIVAT","serviceaftalenote":"Privatpersoner med sundhedsforsikring"},{"serviceaftaleid":"GS-PRIVAT-U-FORS","serviceaftalenote":"Privatpersoner uden sundhedsforsikring"}]]';
$password = 'd7dd2596-ee0d-4035-9f2a-b7aad718ad1e';
$method = 'aes-256-cbc';

// Must be exact 32 chars (256 bit)
$password = substr(hash('sha256', $password, true), 0, 32);
echo "Password:" . $password . "\n";

// IV must be exact 16 chars (128 bit)
$iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

// av3DYGLkwBsErphcyYp+imUW4QKs19hUnFyyYcXwURU=
$encrypted = base64_encode(openssl_encrypt($plaintext, $method, $password, true, $iv));
// My secret message 1234
$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $password, true, $iv);

echo 'plaintext=' . $plaintext . "\n";
echo 'cipher=' . $method . "\n";
echo 'encrypted to: ' . $encrypted . "\n";
echo 'decrypted to: ' . $decrypted . "\n\n";
echo 'vi to: ' . $iv . "\n\n";

?>

