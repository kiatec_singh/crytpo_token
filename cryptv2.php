<?php
use Nullix\CryptoJsAes\CryptoJsAes;
require "src/CryptoJsAes.php";

//$key previously generated safely, ie: openssl_random_pseudo_bytes

$key='3b541db1-8742-4110-abc8-027c0ac71bb7';
$parametre['apiKey']='00d82dc1-c056-4889-a4f1-5af8d9e23886';
$parametre['brugerUuid']='80457b8c-c47f-47ca-a0c5-d6965a3cd02a';
$parametre['cpr']='0902991234';
$parametre['fornavn']='Erik';
$parametre['efternavn']='Helweg Larsen';
$parametre['email']='ehl@kiatec.dk';
$parametre['telefon']='61502371';


$plaintext = http_build_query($parametre);


echo "før dekryptering \n\n";

echo $plaintext."\n";
echo "\n";
// encrypt
$encrypted = CryptoJsAes::encrypt($plaintext, $key);
// something like: {"ct":"g9uYq0DJypTfiyQAspfUCkf+\/tpoW4DrZrpw0Tngrv10r+\/yeJMeseBwDtJ5gTnx","iv":"c8fdc314b9d9acad7bea9a865671ea51","s":"7e61a4cd341279af"}
echo "Encrypted: ";
echo $encrypted;
echo "\n";
echo "\n";

// decrypt
$decrypted = CryptoJsAes::decrypt($encrypted, $key);
// echo "Encrypted: " . $encrypted . "\n";
echo "Decrypted: " . print_r($decrypted, true) . "\n";

?>



