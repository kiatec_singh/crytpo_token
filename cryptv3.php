<?php
use Nullix\CryptoJsAes\CryptoJsAes;
require "src/CryptoJsAes.php";
header('Access-Control-Allow-Origin: *');

    if (isset($_POST['action']) && !empty($_POST['action'])) {
    $actioname = $_POST['action'];
    $value = $_POST['value'];
    switch ($actioname) {
        case "cryptoJsAesDecrypt":
        cryptoJsAesDecrypt($value);
            break;
        case "cryptoJsAesEncrypt":
        cryptoJsAesEncrypt($value);
            break;
        case "default":
            break;
    }  
    }

function cryptoJsAesDecrypt($value){
    $key = '3b541db1-8742-4110-abc8-027c0ac71bb7';
    $decrypted = CryptoJsAes::decrypt($value, $key);
    print_r(json_encode($decrypted, true));
}
/**
* Encrypt value to a cryptojs compatiable json encoding string
* @param mixed $passphrase
* @param mixed $value
* @return string
*/
function cryptoJsAesEncrypt($value){
    $key = '3b541db1-8742-4110-abc8-027c0ac71bb7';
    $decrypted = CryptoJsAes::encrypt($value, $key);
    print_r(json_encode($decrypted, true));
}
?>
