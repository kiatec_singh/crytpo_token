<?php
use Nullix\CryptoJsAes\CryptoJsAes;
require "src/CryptoJsAes.php";
header('Access-Control-Allow-Origin: *');

    if (isset($_POST['action']) && !empty($_POST['action'])) {
    $actioname = $_POST['action'];
    $value = $_POST['value'];
    switch ($actioname) {
        case "cryptoJsAesDecrypt":
        cryptoJsAesDecrypt($value);
            break;
        case "cryptoJsAesEncrypt":
        cryptoJsAesEncrypt($value);
            break;
        case "default":
            break;
    }  
    }

function cryptoJsAesDecrypt($value){
    $key = '654425fe-d87f-47af-9384-cf4e1e2eff51';
    $decrypted = CryptoJsAes::vitaldecrypt($value, $key);
    print_r(json_encode($decrypted, true));
}
/**
* Encrypt value to a cryptojs compatiable json encoding string
* @param mixed $passphrase
* @param mixed $value
* @return string
*/
function cryptoJsAesEncrypt($value){
    $key = '654425fe-d87f-47af-9384-cf4e1e2eff51e';
    $decrypted = CryptoJsAes::vitalencrypt($value, $key);
    print_r(json_encode($decrypted, true));
}
?>
